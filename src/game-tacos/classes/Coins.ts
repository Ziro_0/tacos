import Customer from "./Customer";

export default class Coins extends Phaser.Image {
    onCollected: Phaser.Signal;
    _slotIndex: number;
    _value: number;

    /**
     * @constructor
     */
    constructor(game: Phaser.Game, customer: Customer, value: number) {
        super(game, 0, 0, 'atlas', 'coins');
        this._init(customer);
        
        this._value = value;
        this._slotIndex = customer.slotIndex;
    }

    destroy() {
        this.onCollected.dispose();
        super.destroy();
    }

    get value(): number {
        return(this._value);
    }

    get slotIndex(): number {
        return(this._slotIndex);
    }

    /**
     * @private
     */
    _collect() {
        const jData: any = this.game.cache.getJSON('items');
        const targetPosition: Phaser.Point = new Phaser.Point(
            jData.coins.collectTargetPosition.x,
            jData.coins.collectTargetPosition.y
        );

        const DURATION = 300;
        const tween = this.game.tweens.create(this)
            .to({
                x: targetPosition.x,
                y: targetPosition.y,
                alpha: 0
            },
            DURATION,
            Phaser.Easing.Circular.Out)

            .start();

        tween.onComplete.addOnce(this._onCollectTwnComplete, this);
    }

    /**
     * @private
     */
    _init(customer: Customer) {
        this.onCollected = new Phaser.Signal();

        this._initPosition(customer);
        this._initInput();
    }

    /**
     * @private
     */
    _initInput() {
        this.inputEnabled = true;
        this.events.onInputDown.addOnce(this._onInputDown, this);
    }

    /**
     * @private
     */
    _initPosition(customer: Customer) {
        const jData: any = this.game.cache.getJSON('items');
        const jDataCoinsYPosition: number = jData.coins.yPosition;
        this.x = customer.centerX - this.width / 2;
        this.y = jDataCoinsYPosition;
    }

    /**
     * @private
     */
    _onCollectTwnComplete() {
        this.destroy();
    }

    /**
     * @private
     */
    _onInputDown(object: any, pointer: Phaser.Pointer, money: number) {
        this.onCollected.dispatch(this);
        this._collect();
    }
}
