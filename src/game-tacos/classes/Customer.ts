import CustomerFrameData from "./CustomerFrameData";
import { BeverageKey } from "./ItemKeys";
import TacoIngredient from "./TacoIngredient";

export default class Customer extends Phaser.Image {
    static _framesDataMap: Map<string, CustomerFrameData[]> = new Map();
    static _atlases: Map<string, Phaser.Image> = new Map();
    static readonly BUBBLE_IMAGE_SCALE = 0.72;

    //game: Phaser.Game;
    customerKey: string;
    
    onComplete: Phaser.Signal;

    _bubbleOrderItemImages: TacoIngredient[];
    _orderItemKeys: string[];

    _bubbleImage: Phaser.Image;
    _timerBar: Phaser.Image;
    _bmpData: Phaser.BitmapData;
    _timer: Phaser.Timer;

    _xStart: number;
    _xEnd: number;
    _originalNumberOfOrderItems: number;
    _slotIndex: number;
    _time: number;
    _isComingFromLeft: boolean;
    _isWaiting: boolean;
    _isTutorialMode: boolean;

    /**
     * @constructor
     */
    constructor(game: Phaser.Game, customerKey: string, orderItemKeys: string[],
    isComingFromLeft: boolean, slotIndex: number, time: number, customersGroup: Phaser.Group,
    mask: Phaser.Graphics, isTutorialMode: boolean = false) {
        super(game, 0, 0, null);

        this.customerKey = customerKey;
        this._orderItemKeys = orderItemKeys;
        this._isComingFromLeft = isComingFromLeft;
        this._slotIndex = slotIndex;
        this._originalNumberOfOrderItems = this._orderItemKeys.length;
        this._isTutorialMode = isTutorialMode;
        this._time = time;

        this.onComplete = new Phaser.Signal();

        this._init(customersGroup, mask);
        this.mask = mask;
    }

    static ClearCaches() {
        if (Customer._framesDataMap) {
            Customer._framesDataMap.clear();
        }

        if (Customer._atlases) {
            Customer._atlases.forEach(function(image) {
                image.destroy();
            });
            Customer._atlases.clear();
        }
    }

    destroy() {
        this.onComplete.dispose();
        this._killTheTime();
        this._bmpData.destroy();
        this.destroyAllBubbleItemsAndImage();
        super.destroy();
    }

    destroyAllBubbleItemsAndImage() {
        this._destroyAllBubbleItems();
        this._destroyBubbleImage();
    }

    giveBeverage() {
        if (!this._removeItemFromOrder(BeverageKey)) {
            this._orderFail();
            return;
        }

        if (this._isOrderComplete()) {
            this._orderSuccess();
        }
    }

    giveTaco(ingredientKeys: string[]) {
        if (!ingredientKeys) {
            return; //sanity check
        }

        if (this._isTacoCorrect(ingredientKeys)) {
            this._removeTacoIngredientsFromOrder(ingredientKeys);
            if (this._isOrderComplete()) {
                this._orderSuccess();
            }
        } else {
            this._orderFail();
        }
    }

    get isWaiting(): boolean {
        return(this._isWaiting);
    }

    presentOrder(useTimer: boolean) {
        this._isWaiting = true;
        this._presentOrder();
        this._startWaitingAnimation();

        if (useTimer) {
            this._initTimer();
        }
    }

    get slotIndex(): number {
        return(this._slotIndex);
    }

    update() {
        this._updateTimerBar();
        super.update();
    }

    /**
     * @private
     */
    _createOrderBubbleImage() {
        this._bubbleImage = this.game.add.image(0, 0, 'atlas', 'order-bubble');
        this._bubbleImage.scale.set(Customer.BUBBLE_IMAGE_SCALE);
        
        this._bubbleImage.x = this.x +
            (this.width - this._bubbleImage.width) / 2;
        
        const HEIGHT_OFFSET = 20;
        this._bubbleImage.y = this.y - this._bubbleImage.height + HEIGHT_OFFSET;
    }

    /**
     * @private
     */
    _createOrderBubbleItem(jDataIconPosition: number[], orderItemKey: string) {
        const bubbleItemPoint: Phaser.Point = new Phaser.Point(
            jDataIconPosition[0],
            jDataIconPosition[1]);
        
        const bubbleItemImage = this.game.add.image(
            this._bubbleImage.x + bubbleItemPoint.x,
            this._bubbleImage.y + bubbleItemPoint.y,
            'atlas',
            `bubble-item-${orderItemKey}`);
        bubbleItemImage.scale.set(Customer.BUBBLE_IMAGE_SCALE);

        this._bubbleOrderItemImages.push(new TacoIngredient(orderItemKey, bubbleItemImage));
    }

    /**
     * @private
     */
    _createOrderBubbleItems() {
        this._bubbleOrderItemImages = [];

        const CLONE: boolean = true;
        const jData: any = this.game.cache.getJSON('customers', CLONE);
        const jOrderIconPositions: number[][] = jData.orderIconPositions;
        for (var index = 0; index < this._orderItemKeys.length; index++) {
            const jDataIconPosition: number[] = jOrderIconPositions[index];
            const orderItemKey = this._orderItemKeys[index];
            this._createOrderBubbleItem(jDataIconPosition, orderItemKey);
        }
    }

    /**
     * @private
     */
    _calcTargetPositions(jCustomerData: any) {
        const xTarget = jCustomerData.targetPositions[this.slotIndex];
        if (this._isComingFromLeft) {
            this._xStart = xTarget - this.game.world.width - this.width;
            this._xEnd = xTarget + this.game.world.width + this.width;
        } else {
            this._xStart = xTarget + this.game.world.width + this.width;
            this._xEnd = xTarget - this.game.world.width - this.width;
        }
    }

    /**
     * @private
     */
    _destroyAllBubbleItems() {
        if (!this._bubbleOrderItemImages) {
            return;
        }

        this._bubbleOrderItemImages.forEach(function(tacoIngredient) {
            tacoIngredient.image.destroy();
        }, this);

        this._bubbleOrderItemImages.length = 0;
    }

    /**
     * @private
     */
    _destroyBubbleImage() {
        if (this._bubbleImage) {
            this._bubbleImage.destroy();
            this._bubbleImage = null;
        }
    }

    /**
     * @private
     */
    _doArraysMatch(array1: any[], array2: any[]): boolean {
        if (!array1 || !array2) {
            return(false);
        }

        const length = array1.length;
        if (length !== array2.length) {
            return(false);
        }

        const a1 = array1.concat();
        a1.sort();
        
        const a2 = array2.concat();
        a2.sort();

        for (let index = length - 1; index >= 0; index--) {
            if (a1[index] !== a2[index]) {
                return(false);
            }
        }

        return(true);
    }

    /**
     * @private
     */
    _drawFrame(frameIndex: number) {
        const framesData: CustomerFrameData[] = Customer._framesDataMap.get(this.customerKey);
        if (!framesData) {
            return;
        }

        const frameData: CustomerFrameData = framesData[frameIndex];
        if (!frameData) {
            return;
        }

        let sourceImage = Customer._atlases.get(frameData.atlasKey);
        if (!sourceImage) {
            sourceImage = this.game.make.image(0, 0, frameData.atlasKey);
            Customer._atlases.set(frameData.atlasKey, sourceImage);
        }

        this._bmpData.copyRect(sourceImage, frameData.area);
    }

    _getBubbleOrderItemImageIndex(itemKey: string): number {
        for (let index = 0; index < this._bubbleOrderItemImages.length; index++) {
            const tacoIngredient = this._bubbleOrderItemImages[index];
            if (tacoIngredient.key === itemKey) {
                return(index);
            }
        }

        return(-1);
    }

    /**
     * @private
     */
    _init(group: Phaser.Group, mask: Phaser.Graphics) {
        const CLONE: boolean = true;
        const jData: any = this.game.cache.getJSON('customers', CLONE);
        const jCustomersData: any = jData.customers;
        const jCustomerData: any = jCustomersData[this.customerKey];

        this._initFrameData(jCustomerData);
        this._initBitmapData(jCustomerData);
        this._initDisplayImage(jCustomerData, group, mask);
        this._calcTargetPositions(jCustomerData);
        this._setStartPosition();
        this._moveToStart(jCustomerData);
    }

    /**
     * @private
     */
    _initBitmapData(jCustomerData: any) {
        const frameWidth: number = jCustomerData.width;
        const frameHeight: number = jCustomerData.height;
        this._bmpData = this.game.make.bitmapData(frameWidth, frameHeight);
        this._drawFrame(0);
    }

    /**
     * @private
     */
    _initDisplayImage(jCustomerData: any, group: Phaser.Group, mask: Phaser.Graphics) {
        this.setTexture(this._bmpData.texture);
        this.y = jCustomerData.yOffset;
        group.add(this);
    }

    /**
     * @private
     */
    _initFrameData(jCustomerData: any) {
        const customerKey = this.customerKey;

        let framesData: CustomerFrameData[] = Customer._framesDataMap.get(customerKey);
        if (framesData) {
            return;
        }

        framesData = [];

        const frameWidth: number = jCustomerData.width;
        const frameHeight: number = jCustomerData.height;

        for (let frameIndex = 0; frameIndex < jCustomerData.frames.length; frameIndex++) {
            const frameGrids: number[][] = jCustomerData.frames[frameIndex];
            const atlasKey = `${customerKey}-${frameIndex}`;
            for (let frameGridIndex = 0; frameGridIndex < frameGrids.length; frameGridIndex++) {
                const frameGrid: number[] = frameGrids[frameGridIndex];
                const column: number = frameGrid[0];
                const row: number = frameGrid[1];
                const x: number = column * frameWidth;
                const y: number = row * frameHeight;
                const area = new Phaser.Rectangle(x, y,frameWidth, frameHeight);

                framesData.push(new CustomerFrameData(area, atlasKey));
            }
        }

        Customer._framesDataMap.set(customerKey, framesData);
    }

    /**
     * @private
     */
    _initTimer() {
        this._initTimerBar();

        const AUTO_DESTROY = true;
        this._timer = this.game.time.create(AUTO_DESTROY);
        this._timer.add(this._time, this._onTimerComplete, this);
        this._timer.start();
    }

    /**
     * @private
     */
    _initTimerBar() {
        const x = this.centerX;
        const y = 455;
        this._timerBar = this.game.add.image(x, y, 'atlas', 'time-bar');
        this._timerBar.anchor.x = 0.5;
    }

    /**
     * @private
     */
    _isOrderComplete(): boolean {
        return(this._orderItemKeys.length === 0);
    }

    /**
     * @private
     */
    _isTacoCorrect(ingredientKeys: string[]): boolean {
        const customerTacoItemsOnly: string[] = this._removeBeverageFromItems();
        return(this._doArraysMatch(ingredientKeys, customerTacoItemsOnly));
    }

    /**
     * @private
     */
    _killTheTime() {
        if (this._timer) {
            this._timer.destroy();
            this._timer = null;
        }

        if (this._timerBar) {
            this._timerBar.destroy();
            this._timerBar = null;
        }
    }

    /**
     * @private
     */
    _moveToEnd() {
        const xTarget = this._xEnd;
        const DURATION = 3000;
        const tween = this.game.tweens.create(this)
            .to({
                x: xTarget
            },
            DURATION,
            Phaser.Easing.Circular.In)

            .start();

        tween.onComplete.addOnce(this._onMoveToEndTwnComplete, this);
    }

    /**
     * @private
     */
    _moveToStart(jCustomerData: any) {
        const xTarget = jCustomerData.targetPositions[this.slotIndex];
        if (this._isTutorialMode) {
            this.x = xTarget;
            return;
        }

        const DURATION = 3000;
        const tween = this.game.tweens.create(this)
            .to({
                x: xTarget
            },
            DURATION,
            Phaser.Easing.Power1)

            .start();

        tween.onComplete.addOnce(this._onMoveToStartTwnComplete, this);
    }

    /**
     * @private
     */
    _onMoveToEndTwnComplete() {
        this.destroy();
    }

    /**
     * @private
     */
    _onMoveToStartTwnComplete() {
        const USE_TIMER = true;
        this.presentOrder(USE_TIMER);
    }

    /**
     * @private
     */
    _onTimerComplete() {
        this._orderFail();
    }

    /**
     * @private
     */
    _orderFail() {
        console.log('fail');
        this._killTheTime();
        this._destroyAllBubbleItems();
        this._destroyBubbleImage();
        this._isWaiting = false;

        const IS_SUCCESSFUL = false;
        this.onComplete.dispatch(this, IS_SUCCESSFUL);

        this._moveToEnd();
    }

    /**
     * @private
     */
    _orderSuccess() {
        console.log('success');
        this._killTheTime();
        this._destroyBubbleImage();
        this._isWaiting = false;

        const IS_SUCCESSFUL = true;
        const money = this._originalNumberOfOrderItems;
        this.onComplete.dispatch(this, IS_SUCCESSFUL, money);

        this._moveToEnd();
    }

    /**
     * @private
     */
    _presentOrder() {
        this._createOrderBubbleImage();
        this._createOrderBubbleItems();
    }

    /**
     * @private
     */
    _removeBeverageFromItems(): string[] {
        const tacoItemsOnly: string[] = this._orderItemKeys.concat();
        const index = tacoItemsOnly.indexOf(BeverageKey);
        if (index > -1) {
            tacoItemsOnly.splice(index, 1);
            this._updateBubbleOrderItems();
        }
        return(tacoItemsOnly);
    }

    /**
     * @private
     */
    _removeItemFromOrder(itemKey: string): boolean {
        const index = this._orderItemKeys.indexOf(itemKey);
        if (index === -1) {
            return(false);
        }

        this._orderItemKeys.splice(index, 1);

        const ingredientIndex = this._getBubbleOrderItemImageIndex(itemKey);
        if (ingredientIndex > -1) {
            const ingredient = this._bubbleOrderItemImages[ingredientIndex];
            if (ingredient.image) {
                ingredient.image.destroy();
                this._bubbleOrderItemImages.splice(ingredientIndex, 1);
            }
        }
        
        return(true);
    }

    /**
     * @private
     */
    _removeTacoIngredientsFromOrder(ingredientKeys: string[]) {
        ingredientKeys.forEach(function(ingredientKey) {
            this._removeItemFromOrder(ingredientKey);
        }, this);
        this._updateBubbleOrderItems();
    }

    /**
     * @private
     */
    _setStartPosition() {
        this.x = this._xStart;
    }

    /**
     * @private
     */
    _startWaitingAnimation() {
    }

    /**
     * @private
     */
    _updateBubbleOrderItems() {
        const jData: any = this.game.cache.getJSON('customers');
        const jOrderIconPositions: number[][] = jData.orderIconPositions;

        this._bubbleOrderItemImages.forEach(function(ingredient, index) {
            const jDataIconPosition: number[] = jOrderIconPositions[index++];
            const bubbleItemPoint: Phaser.Point = new Phaser.Point(
                jDataIconPosition[0],
                jDataIconPosition[1]);
            
            ingredient.image.x = this._bubbleImage.x + bubbleItemPoint.x;
            ingredient.image.y = this._bubbleImage.y + bubbleItemPoint.y;
        }, this);
    }

    /**
     * @private
     */
    _updateTimerBar() {
        if (!this._timer || !this._timerBar) {
            return;
        }

        const ratio = this._timer.duration / this._time;
        if (isNaN(ratio)) {
            return;
        }

        this._timerBar.scale.x = ratio;
    }
}
