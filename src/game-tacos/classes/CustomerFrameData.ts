export default class CustomerFrameData {
    area: Phaser.Rectangle;
    atlasKey: string;

    /**
     * @constructor
     */
    constructor(area: Phaser.Rectangle, atlasKey: string) {
        this.area = area;
        this.atlasKey = atlasKey;
    }
}
