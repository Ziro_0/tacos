import Customer from "./Customer";

export default class CustomerIdOfPointerOverCustomer {
    game: Phaser.Game;
    
    _customersGroup: Phaser.Group;
    _customersMaskRect: Phaser.Rectangle;

    /**
     * @constructor
     */
    constructor(game: Phaser.Game, customersGroup: Phaser.Group,
    customersMaskRect: Phaser.Rectangle) {
        this.game = game;
    
        this._customersGroup = customersGroup;
        this._customersMaskRect = customersMaskRect;
    }

    getCustomerSlotIndex(): number {
        if (!this._isPointerWithinCustomerMask()) {
            return(-1);
        }

        const customers: Customer[] = this._getCustomersOverPointer();
        if (customers.length === 0) {
            return(-1);
        }

        const customer = this._getCustomerClosestToPointerPosition(customers);
        return(customer.slotIndex);
    }

    /**
     * @private
     */
    get _activePointerPosition(): Phaser.Point {
        const pointerPosition = this.game.input.activePointer.position;
        return(new Phaser.Point(pointerPosition.x, pointerPosition.y));
    }

    /**
     * @private
     */
    _getCustomerClosestToPointerPosition(customers: Customer[]): Customer {
        const pointerPosition = this._activePointerPosition;
        let closestCustomertDistance: number;
        let closestCustomer: Customer = null;

        customers.forEach(function(customer) {
            if (!closestCustomer) {
                closestCustomer = customer;
                closestCustomertDistance = Phaser.Math.distance(
                    pointerPosition.x, pointerPosition.y,
                    closestCustomer.centerX, closestCustomer.centerY);
            } else {
                const otherCustomerDistance  = Phaser.Math.distance(
                    pointerPosition.x, pointerPosition.y,
                    customer.centerX, customer.centerY);

                if (otherCustomerDistance < closestCustomertDistance) {
                    closestCustomer = customer;
                    closestCustomertDistance = otherCustomerDistance;
                }
            }
        });
        return(closestCustomer);
    }

    /**
     * @private
     */
    _getCustomersOverPointer(): Customer[] {
        const customers: Customer[] = [];
        const pointerPosition = this._activePointerPosition;

        for (let index = 0; index < this._customersGroup.length; index++) {
            const customer = this._customersGroup.getAt(index) as Customer;
            if (!customer.isWaiting) {
                continue;
            }

            const bounds = customer.getBounds();
            if (bounds.contains(pointerPosition.x, pointerPosition.y)) {
                customers.push(customer);
            }
        }

        return(customers);
    }

    /**
     * @private
     */
    _isPointerWithinCustomerMask(): boolean {
        const pointerPosition = this._activePointerPosition;
        return(this._customersMaskRect.contains(
            pointerPosition.x,
            pointerPosition.y));
    }
}
