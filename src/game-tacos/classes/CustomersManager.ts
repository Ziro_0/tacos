import { IngredientKeys, BeverageKey } from "./ItemKeys";

export default class CustomersManager {
    static readonly NUM_CUSTOMER_SLOTS = 3;
    
    game: Phaser.Game;

    onNewCustomer: Phaser.Signal;

    minCustomerSpawnRateMs: number;
    maxCustomerSpawnRateMs: number;
    minCustomerTimeMs: number;
    maxCustomerTimeMs: number;

    /** data: customer key */
    _customerSlots: string[];
    
    _timer: Phaser.Timer;

    /**
     * @constructor
     */
    constructor(game: Phaser.Game, minCustomerSpawnRateMs: number,
    maxCustomerSpawnRateMs: number, minCustomerTimeMs: number,
    maxCustomerTimeMs: number, firstCustomerSpawnTimeMs: number) {
        this.game = game;
        this.minCustomerSpawnRateMs = minCustomerSpawnRateMs;
        this.maxCustomerSpawnRateMs = maxCustomerSpawnRateMs;
        this.minCustomerTimeMs = minCustomerTimeMs;
        this.maxCustomerTimeMs = maxCustomerTimeMs;

        this.onNewCustomer = new Phaser.Signal();
        this._customerSlots = [];
        this._customerSlots.length = CustomersManager.NUM_CUSTOMER_SLOTS;

        this._initTimer();
        this._startSpawner(firstCustomerSpawnTimeMs);
    }

    clearCustomerSlot(index: number) {
        index = Math.floor(Phaser.Math.clamp(index, 0, CustomersManager.NUM_CUSTOMER_SLOTS - 1));
        this._customerSlots[index] = null;
    }

    destroy() {
        this._timer.destroy();
    }

    presentTutorialCustomer(customerId: string, slotIndex: number, orderItems: string[]) {
        if (this._customerSlots[slotIndex]) {
            //slot already occupied
            return(false);
        }

        const IS_TUTORIAL_MODE = true;
        this._customerSlots[slotIndex] = customerId;
        this.onNewCustomer.dispatch(customerId, slotIndex, orderItems, 0, IS_TUTORIAL_MODE);
        return(true);
    }

    restartSpawner() {
        this._timer.removeAll();
        this._startSpawner();
    }

    start() {
        this._timer.start();
    }
    
    stop() {
        this._timer.stop();
    }

    /**
     * @private
     */
    _createRandomOrderItemKeys(): string[] {
        const availableKeys: string[] = IngredientKeys.concat();
        availableKeys.push(BeverageKey);

        const MAX_ITEMS_IN_ORDER = 4;
        const numberItemsInOrder = Math.floor(Math.random() * MAX_ITEMS_IN_ORDER) + 1;
        const finalKeys: string[] = [];
        
        for (var index = 0; index < numberItemsInOrder; index++) {
            const key = Phaser.ArrayUtils.getRandomItem(availableKeys);
            if (key) {
                finalKeys.push(key);
            }
        }

        return(finalKeys);
    }

    /**
     * @private
     */
    _dispatchNewCustomerRequest(customerSlotIndex: number) {
        const customerId = this._getRandomCustomerId();
        this._customerSlots[customerSlotIndex] = customerId;
        const orderItemKeys = this._createRandomOrderItemKeys();
        const customerTime = this._getRandomCustomerTime();
        this.onNewCustomer.dispatch(customerId, customerSlotIndex, orderItemKeys, customerTime);
    }

    /**
     * @private
     */
    _getRandomCustomerId(): string {
        const jData: any = this.game.cache.getJSON('customers');
        const keys: string[] = Object.keys(jData.customers);
        return(Phaser.ArrayUtils.getRandomItem(keys));
    }

    /**
     * @private
     */
    _getRandomCustomerTime(): number {
        return(Phaser.Math.between(
            this.minCustomerTimeMs,
            this.maxCustomerTimeMs));
    }

    /**
     * @private
     */
    _getRandomFreeSlotIndex(): number {
        const freeSlotIndexes: number[] = [];

        for (let index = 0; index < this._customerSlots.length; index++) {
            if (!this._customerSlots[index]) {
                freeSlotIndexes.push(index);
            }
        }

        if (freeSlotIndexes.length === 0) {
            return(-1);
        }

        return(Phaser.ArrayUtils.getRandomItem(freeSlotIndexes));
    }

    /**
     * @private
     */
    _initTimer() {
        const AUTO_DESTROY = false;
        this._timer = this.game.time.create(AUTO_DESTROY);
    }

    /**
     * @private
     */
    _onTimerSpwan() {
        const customerSlotIndex = this._getRandomFreeSlotIndex();
        if (customerSlotIndex > -1) {
            this._dispatchNewCustomerRequest(customerSlotIndex);
        }

        this._startSpawner();
    }

    /**
     * @private
     */
    _startSpawner(firstCustomerSpawnTimeMs?: number) {
        const delay = firstCustomerSpawnTimeMs !== undefined ?
            firstCustomerSpawnTimeMs :
            Phaser.Math.between(
                this.minCustomerSpawnRateMs,
                this.maxCustomerSpawnRateMs);
        this._timer.add(delay, this._onTimerSpwan, this);
    }
}
