export default class Heart extends Phaser.Sprite {
    game: any;
    anchor: any;
    scale: any;
    num: any;

    constructor(game, lives) {
        let x = game.world.centerX;
        let y = game.world.centerY;
        super(game, x, y, 'heart');
        this.game.groups.gui.add(this);

        this.anchor.setTo(0.5);
        this.scale.setTo(0.5);

        this.num = this.game.add.text(0, -60, lives);
        this.num.anchor.setTo(0.5);
        this.num.fontSize = 60;
        this.num.fill = '#e75a70';
        this.addChild(this.num);

        this.game.add
            .tween(this.scale)
            .to({ x: 1, y: 1 }, 250, 'Bounce', true)
            .onComplete.add(() => {
                this.game.add
                    .tween(this)
                    .to({ y: '-300', alpha: 0 }, 500, 'Linear', true)
                    .onComplete.add(() => {
                        this.destroy();
                    });
            });
    }
}
