export type ItemShape = Phaser.Circle | Phaser.Rectangle | Phaser.Point;

export default class ItemData {
    key: string;
    shapeType: string;
    shape: ItemShape;
    width: number;
    height: number;

    constructor(key: string, position: any) {
        this.key = key;
        this._initPosition(position);
    }

    /**
     * @private
     */
    _createCircle(position: any): Phaser.Circle {
        const missingProps: string[] =
            this._getMissingProps(position, ["x", "y", "diameter"]);

        if (missingProps) {
            console.warn('Missing properties for circle position: ' + missingProps.join(', '));
            return(null);
        }

        this.width = this.height = position.diameter;
        return(new Phaser.Circle(position.x, position.y, position.diameter));
    }

    /**
     * @private
     */
    _createPoint(position: any): Phaser.Point {
        const missingProps: string[] =
            this._getMissingProps(position, ["x", "y"]);

        if (missingProps) {
            console.warn('Missing properties for point position: ' + missingProps.join(', '));
            return(null);
        }

        this.width = this.height = 1;
        return(new Phaser.Point(position.x, position.y));
    }

    /**
     * @private
     */
    _createRect(position: any): Phaser.Rectangle {
        const missingProps: string[] =
            this._getMissingProps(position, ["x", "y", "width", "height"]);

        if (missingProps) {
            console.warn('Missing properties for rect position: ' + missingProps.join(', '));
            return(null);
        }

        this.width = position.width;
        this.height = position.height;
        return(new Phaser.Rectangle(position.x, position.y, position.width, position.height));
    }

    /**
     * @private
     */
    _getMissingProps(position: any, propNames: string[]): string[] {
        var missingProps: string[] = null;

        propNames.forEach(function(name) {
            if (!(name in position)) {
                missingProps = missingProps || [];
                missingProps.push(name);
            }
        });

        return(missingProps);
    }

    /**
     * @private
     */
    _initPosition(position: any) {
        if (!position) {
            return;
        }

        let type: string = position.type;
        if (!type) {
            console.warn(`No "type" defined for position with key: "${this.key}"`);
            return;
        }

        switch (type) {
            case "circle":
                this.shape = this._createCircle(position);
                break;
            
            case "point":
                this.shape = this._createPoint(position);
                break;

            case "rect":
            case "rectangle":
                type = "rect";
                this.shape = this._createRect(position);
                break;

            default:
                console.warn(`No "type" defined for position with key: "${this.key}"`);
                return;
        }

        this.shapeType = type;
    }
}
