export const Beans = "beans";
export const Corn = "corn";
export const GreenSause = "green-sauce";
export const Meat = "meat";
export const Onion = "onion";
export const Peppers = "peppers";
export const RedSauce = "red-sauce";
export const Tomato = "tomato";
export const YellowSauce = "yellow-sauce";

export const BeverageKey = "beverage";
export const TacoKey = "taco";
export const TrashKey = "trash";

export const IngredientKeys = [
    Beans,
    Corn,
    GreenSause,
    Meat,
    Onion,
    Peppers,
    RedSauce,
    Tomato,
    YellowSauce
];
