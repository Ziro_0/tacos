import ItemData, { ItemShape } from "./ItemData";
import { IngredientKeys, TacoKey, TrashKey, BeverageKey } from "./ItemKeys";
import Trash from "./Trash";
import CustomerIdOfPointerOverCustomer from "./CustomerIdOfPointerOverCustomer";
import TacoIngredient from "./TacoIngredient";

export default class ItemsManager {
    static readonly _transformableProps: string[] = [
        "x", "y", "diameter", "width", "height"
    ];

    game: Phaser.Game;
    
    onCustomerReceivedTaco: Phaser.Signal;
    onCustomerReceivedBeverage: Phaser.Signal;
    onIngredientDroppedOnTaco: Phaser.Signal;
    onTacoDiscarded: Phaser.Signal;

    /** key: item key */
    _ingredientsData: Map<string, ItemData>;
    
    /** key: item key */
    _contacts: Map<string, Phaser.Graphics>

    _tacoIngredients: TacoIngredient[];
    
    _finalTacoIngredientKeys: string[];

    _customerSlotIndexOfPointerOverCustomer: CustomerIdOfPointerOverCustomer;

    _tacoData: ItemData;
    _trashData: ItemData;

    _trash: Trash;

    _itemsGroup: Phaser.Group;
    _activeItemImage: Phaser.Image;
    _onItemDropped: Phaser.Signal;

    /**
     * @constructor
     */
    constructor(game: Phaser.Game, gameScale: number, itemsGroup: Phaser.Group,
    customersGroup: Phaser.Group, customersMaskRect:Phaser.Rectangle) {
        this.game = game;
        this.onCustomerReceivedTaco = new Phaser.Signal();
        this.onCustomerReceivedBeverage = new Phaser.Signal();
        this.onIngredientDroppedOnTaco = new Phaser.Signal();
        this.onTacoDiscarded = new Phaser.Signal();

        this._itemsGroup = itemsGroup;
        this._onItemDropped = new Phaser.Signal();

        this._tacoIngredients = [];
        
        this._customerSlotIndexOfPointerOverCustomer =
            new CustomerIdOfPointerOverCustomer(
                this.game,
                customersGroup,
                customersMaskRect);

        this._initData(gameScale);
        this._createContacts();
        this._initTrash();
    }

    disableAll() {
        this._contacts.forEach(function(contact) {
            if (contact.input) {
                contact.input.enabled = false;
            }
        });
    }

    dispose() {
        this.onCustomerReceivedTaco.dispose();
        this.onCustomerReceivedBeverage.dispose();
        this.onIngredientDroppedOnTaco.dispose();
        this.onTacoDiscarded.dispose();
    }

    enableAll() {
        this._contacts.forEach(function(contact) {
            if (contact.input) {
                contact.input.enabled = true;
            }
        });
    }

    enableOnly(itemKeyToEnable: string) {
        this._contacts.forEach(function(contact, itemKey) {
            if (contact.input) {
                contact.input.enabled = itemKey === itemKeyToEnable;
            }
        });
    }

    getIngredientItemData(key: string) {
        return(this._ingredientsData.get(key));
    }

    get tacoData(): ItemData {
        return(this._tacoData);
    }

    update() {
        if (!this._activeItemImage) {
            return;
        }

        if (this.game.input.activePointer.isUp) {
            this._onItemDropped.dispatch();
        } else {
            this._handleDragActiveItem();
        }
    }

    /**
     * @private
     */
    _addIngredientToTaco(key: string) {
        this._tacoIngredients.push(new TacoIngredient(key, this._activeItemImage));
        this._releaseActiveItem();
        this.onIngredientDroppedOnTaco.dispatch(key);
    }

    /**
     * @private
     */
    _createActiveIngredientImage(itemData: ItemData) {
        this._activeItemImage = this.game.add.image(
            this.game.input.activePointer.x,
            this.game.input.activePointer.y,
            'atlas',
            'drag-item-' + itemData.key,
            this._itemsGroup);
        
        if (!this._activeItemImage) {
            return;
        }

        this._activeItemImage.anchor.set(0.5);
        this._onItemDropped.addOnce(this._onIngredientDropped, this, 0, itemData.key);
    }

    /**
     * @private
     */
    _createActiveBeverageImage() {
        this._activeItemImage = this.game.add.image(
            this.game.input.activePointer.x,
            this.game.input.activePointer.y,
            'atlas',
            'drag-item-' + BeverageKey,
            this._itemsGroup);
        
        if (!this._activeItemImage) {
            return;
        }

        this._activeItemImage.anchor.set(0.5);
        this._onItemDropped.addOnce(this._onBevarageDropped, this);
    }

    /**
     * @private
     */
    _createActiveTacoImage() {
        if (this._tacoIngredients.length === 0) {
            return;
        }

        this._activeItemImage = this.game.add.image(
            this.game.input.activePointer.x,
            this.game.input.activePointer.y,
            'atlas',
            'drag-item-' + TacoKey,
            this._itemsGroup);
        
        if (!this._activeItemImage) {
            return;
        }

        this._activeItemImage.anchor.set(0.5);

        this._createFinalTacoIngredientKeys();
        this._destroyTacoIngredienImages();

        this._onItemDropped.addOnce(this._onTacoDropped, this);
    }

    /**
     * @private
     */
    _createCircleContact(itemData: ItemData): Phaser.Graphics {
        const shape: Phaser.Circle = itemData.shape as Phaser.Circle;
        const circle = this.game.add.graphics(shape.x, shape.y);
        circle.beginFill(0x0000ff, 0);
        circle.drawCircle(0, 0, shape.diameter);
        circle.endFill();
        return(circle);
    }

    /**
     * @private
     */
    _createContacts() {
        this._contacts = new Map();

        const itemDatas: ItemData[] = [];
        this._ingredientsData.forEach(function(itemData) {
            itemDatas.push(itemData);
        });

        itemDatas.push(this._tacoData);

        itemDatas.forEach(function(itemData) {
            let contactShape: Phaser.Graphics = null;
            
            if (itemData.shapeType === "rect") {
                contactShape = this._createRectContact(itemData);
            } else if (itemData.shapeType === "circle") {
                contactShape = this._createCircleContact(itemData);
            }

            if (!contactShape) {
                return;
            }

            contactShape.inputEnabled = true;
            contactShape.events.onInputDown.add(
                this._onContactInputDown, this, 0, itemData);

            this._contacts.set(itemData.key, contactShape);
        }, this);
    }

    /**
     * @private
     */
    _createDraggableItem(itemData: ItemData) {
        if (this._isIngredient(itemData.key)) {
            this._createActiveIngredientImage(itemData);
        } else if (itemData.key === BeverageKey) {
            this._createActiveBeverageImage();
        } else if (itemData.key === TacoKey) {
            this._createActiveTacoImage();
        } else if (itemData.key === TrashKey) {
        }
    }

    /**
     * @private
     */
    _createFinalTacoIngredientKeys() {
        this._finalTacoIngredientKeys = [];
        this._tacoIngredients.forEach(function(tacoIngredient) {
            this._finalTacoIngredientKeys.push(tacoIngredient.key);
        }, this);
    }

    /**
     * @private
     */
    _createRectContact(itemData: ItemData): Phaser.Graphics {
        const shape: Phaser.Rectangle = itemData.shape as Phaser.Rectangle;
        const rect = this.game.add.graphics(shape.x, shape.y);
        rect.beginFill(0x00ff00, 0);
        rect.drawRect(0, 0, shape.width, shape.height);
        rect.endFill();
        return(rect);
    }

    /**
     * @private
     */
    _destroyActiveItemImage() {
        this._activeItemImage.destroy();
        this._activeItemImage = null;
    }

    /**
     * @private
     */
    _destroyTacoIngredienImages() {
        this._tacoIngredients.forEach(function(tacoIngredient) {
            tacoIngredient.image.destroy();
        });

        this._tacoIngredients.length = 0;
    }

    /**
     * @private
     */
    _giveBeverageToCustomer(customerSlotIndex:number) {
        this.onCustomerReceivedBeverage.dispatch(customerSlotIndex);
    }
    
    /**
     * @private
     */
    _giveTacoToCustomer(customerSlotIndex: number) {
        this.onCustomerReceivedTaco.dispatch(customerSlotIndex, this._finalTacoIngredientKeys);
    }

    /**
     * @private
     */
    _handleDragActiveItem() {
        const pointer = this.game.input.activePointer;
        this._activeItemImage.x = pointer.x;
        this._activeItemImage.y = pointer.y;
    }

    /**
     * @private
     */
    _initData(gameScale: number) {
        const CLONE: boolean = true;
        const jData: any = this.game.cache.getJSON('items', CLONE);
        this._initDataIngredients(jData.ingredients, gameScale);
        this._initDataTaco(jData.taco, gameScale);
        this._initDataTrash(jData.trash, gameScale);
    }

    /**
     * @private
     */
    _initDataIngredients(jData: any, gameScale: number) {
        this._ingredientsData = new Map();
        
        Object.keys(jData).forEach(function(key) {
            const jItemData = jData[key];
            const position = jItemData.position;
            if (!position) {
                return;
            }

            this._transformLocation(position, gameScale);
            const itemData = new ItemData(key, position);
            if (itemData.shape) {
                this._ingredientsData.set(key, itemData);
            }
        }, this);
    }

    /**
     * @private
     */
    _initDataTaco(jData: any, gameScale: number) {
        const position = jData.position;
        if (position) {
            this._transformLocation(position, gameScale);
            this._tacoData = new ItemData(TacoKey, position);
        }
    }

    /**
     * @private
     */
    _initDataTrash(jData: any, gameScale: number) {
        this._transformLocation(jData, gameScale);
        this._trashData = new ItemData(TrashKey, jData.position);
    }

    /**
     * @private
     */
    _initTrash() {
        this._trash = new Trash(this.game, this._trashData);
        this.game.world.add(this._trash);
    }

    /**
     * @private
     */
    _isIngredient(key: string): boolean {
        return(IngredientKeys.indexOf(key) > -1);
    }

    /**
     * @private
     */
    _isPointerOverShape(shape: ItemShape): boolean {
        if (shape instanceof Phaser.Circle) {
            const circle: Phaser.Circle = shape as Phaser.Circle;
            return(circle.contains(
                this.game.input.activePointer.x,
                this.game.input.activePointer.y));
        } else if (shape instanceof Phaser.Rectangle) {
            const rect: Phaser.Rectangle = shape as Phaser.Rectangle;
            return(rect.contains(
                this.game.input.activePointer.x,
                this.game.input.activePointer.y));
        }
        return(false);
    }

    /**
     * @private
     */
    _isPointerOverTortilla():boolean {
        return(this._isPointerOverShape(this._tacoData.shape));
    }
        
    /**
     * @private
     */
    _isTransformableProp(key: string): boolean {
        return(ItemsManager._transformableProps.indexOf(key) > -1);
    }

    /**
     * @private
     */
    _onBevarageDropped() {
        const customerSlotIndex = this._customerSlotIndexOfPointerOverCustomer.getCustomerSlotIndex();
        if (customerSlotIndex > -1) {
            this._giveBeverageToCustomer(customerSlotIndex);
        } else {
            this._playTrashAnimation();
        }

        this._destroyActiveItemImage();
    }

    /**
     * @private
     */
    _onContactInputDown(target: any, pointer: Phaser.Pointer, itemData: ItemData) {
        if (!this._activeItemImage) {
            this._createDraggableItem(itemData);
        }
    }

    /**
     * @private
     */
    _onIngredientDropped(key: string) {
        if (this._isPointerOverTortilla()) {
            this._addIngredientToTaco(key);
        } else {
            this._destroyActiveItemImage();
        }
    }

    /**
     * @private
     */
    _onTacoDropped() {
        const customerSlotIndex = this._customerSlotIndexOfPointerOverCustomer.getCustomerSlotIndex();
        if (customerSlotIndex > -1) {
            this._giveTacoToCustomer(customerSlotIndex);
        } else {
            this._playTrashAnimation();
            this.onTacoDiscarded.dispatch();
        }

        this._destroyActiveItemImage();
    }
    
    /**
     * @private
     */
    _playTrashAnimation() {
        this._trash.open();
    }

    /**
     * @private
     */
    _releaseActiveItem() {
        this._activeItemImage = null;
    }

    /**
     * @private
     */
    _transformLocation(position_in_out: any, gameScale: number): any {
        Object.keys(position_in_out).forEach(function(key) {
            if (this._isTransformableProp(key)) {
                let value: number = position_in_out[key];
                position_in_out[key] = value * gameScale;
            }
        }, this);
    }
}
