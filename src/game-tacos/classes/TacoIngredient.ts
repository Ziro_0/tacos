export default class TacoIngredient {
    key: string;
    image: Phaser.Image;

    /**
     * @constructor
     */
    constructor(key: string, image: Phaser.Image) {
        this.key = key;
        this.image = image;
    }
}
