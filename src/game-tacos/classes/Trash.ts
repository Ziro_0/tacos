import ItemData from "./ItemData";

export default class Trash extends Phaser.Image {
    static readonly _animFrames = [
        'trash-can-0',
        'trash-can-1',
        'trash-can-2',
        'trash-can-3',
        'trash-can-2',
        'trash-can-1',
        'trash-can-0'
    ];

    _openTimer: Phaser.Timer;
    _openTimerEvent: Phaser.TimerEvent;
    _frameIndex: number;

    /**
     * @constructor
     */
    constructor(game: Phaser.Game, trashData: ItemData) {
        super(game, trashData.shape.x, trashData.shape.y, 'trash',
            Trash._animFrames[0]);

        this._openTimer = this.game.time.create(false);
        this._openTimer.start();
    }

    open() {
        this._setFrameInage(0);

        const DELAY = 75;

        this._openTimer.removeAll();
        this._openTimerEvent = this._openTimer.repeat(
            DELAY,
            Trash._animFrames.length,
            this._onTimer, this);
    }

    /**
     * @private
     */
    _onTimer() {
        const index = this._frameIndex + 1;
        if (index < Trash._animFrames.length) {
            this._setFrameInage(index);
        }
    }

    /**
     * @private
     */
    _setFrameInage(index: number) {
        this._frameIndex = index;
        this.frameName = Trash._animFrames[this._frameIndex];
    }
}
