import CustomersManager from "./CustomersManager";
import Customer from "./Customer";
import GameState from "../states/GameState";
import ItemsManager from "./ItemsManager";
import TutorialArrow from "./TutorialArrow";
import ItemData from "./ItemData";
import { BeverageKey, TacoKey } from "./ItemKeys";

export default class Tutorial {
    static readonly _CUSTOMER_SLOT_INDEX = 1;

    onComplete: Phaser.Signal;

    _gameState: GameState;
    _customersManager: CustomersManager;
    _itemsManager: ItemsManager;
    _tutorialArrow: TutorialArrow;
    
    _game: Phaser.Game;
    _customer: Customer;

    _bubbleImage: Phaser.Image;
    _timer: Phaser.Timer;
    _tacoPosition: Phaser.Point;
    _beveragePosition: Phaser.Point;
    _customerPosition: Phaser.Point;

    _currentHighlightedItemIndex: number;

    _wasBeverageReceived: boolean;
    _wasTacoDiscarded: boolean;

    /**
     * @constructor
     */
    constructor(gameState: GameState, customersManager: CustomersManager,
    _itemsManager: ItemsManager) {
        this._gameState = gameState;
        this._game = this._gameState.game;
        
        this._itemsManager = _itemsManager;
        this._itemsManager.disableAll();
        this._addItemsManagerSignals();

        this._customersManager = customersManager;

        this._initTacoPosition();
        this._initBeveragePosition();

        this.onComplete = new Phaser.Signal();
        
        const AUTO_DESTROY = false;
        this._timer = this._game.time.create(AUTO_DESTROY);
        this._timer.start();
    }

    start() {
        this._presentCustomer();

        this._timer.add(2000, function() {
            this._onTimerPresentCustomerComplete();
        }, this);
    }

    dispose() {
        this._removeItemsManagerSignals();
        this._destroyBubbleImage();
        this._removeItemsManagerSignals();
        this._destroyTutorialArrow();
        this.onComplete.dispose();
    }

    /**
     * @private
     */
    _addItemsManagerSignals() {
        this._itemsManager.onIngredientDroppedOnTaco.add(
            this._itemsManagerOnIngredientDroppedOnTaco, this);

        this._itemsManager.onCustomerReceivedBeverage.add(
            this._itemsManagerOnCustomerReceivedBeverage, this);

        this._itemsManager.onTacoDiscarded.add(
            this._itemsManagerOnTacoDiscarded, this);
    }

    /**
     * @private
     */
    _destroyBubbleImage() {
        if (this._bubbleImage) {
            this._bubbleImage.destroy();
            this._bubbleImage = null;
        }
    }

    /**
     * @private
     */
    _destroyTutorialArrow() {
        if (this._tutorialArrow) {
            this._tutorialArrow.destroy();
            this._tutorialArrow = null;
        }
    }

    /**
     * @private
     */
    _getOrderItemPosition(itemKey: string): Phaser.Point {
        const itemData = this._itemsManager.getIngredientItemData(itemKey);
        const position = new Phaser.Point(
            itemData.shape.x + itemData.width / 2,
            itemData.shape.y + itemData.height / 2);
        return(position);
    }

    /**
     * @private
     */
    _getTutorialOrderItems(): string[] {
        const jData: any = this._game.cache.getJSON('items');
        const items: string[] = jData.tutorialItems.concat();
        
        if (this._wasBeverageReceived && this._wasTacoDiscarded) {
            const beverageIndex = items.indexOf(BeverageKey);
            items.splice(beverageIndex, 1);
        }

        return(items);
    }

    /**
     * @private
     */
    _handleNextItem(itemKey: string) {
        if (this._highlightNextOrderItem()) {
            return;
        }

        this._itemsManager.disableAll();
        this._tutorialArrow.visible = false;
        this._customer.destroyAllBubbleItemsAndImage();

        this._presentBubble(2, 310, 5);
        
        const e = this._timer.add(2000, function() {
            this._onTimerPresentGiveTacoComplete();
        }, this);
    }

    /**
     * @private
     */
    _highlightNextOrderItem(): boolean {
        return(this._highlightOrderItem(this._currentHighlightedItemIndex + 1));
    }

    /**
     * @private
     */
    _highlightOrderItem(itemIndex: number): boolean {
        const tutorialOrderItems: string[] = this._getTutorialOrderItems();
        const itemKey = tutorialOrderItems[itemIndex];
        if (!itemKey) {
            return(false);
        }

        this._currentHighlightedItemIndex = itemIndex;

        if (!this._tutorialArrow) {
            this._tutorialArrow = new TutorialArrow(this._game);
        }

        let fromPosition: Phaser.Point;
        let toPosition: Phaser.Point;

        if (itemKey === BeverageKey) {
            fromPosition = this._beveragePosition;
            toPosition = this._customerPosition;
        } else {
            fromPosition = this._getOrderItemPosition(itemKey);
            toPosition = this._tacoPosition;
        }

        this._tutorialArrow.presentFromPosition(fromPosition, toPosition);

        this._itemsManager.enableOnly(itemKey);
        return(true);
    }

    /**
     * @private
     */
    _initBeveragePosition() {
        const itemData: ItemData =
            this._itemsManager.getIngredientItemData(BeverageKey);

        this._beveragePosition = new Phaser.Point(
            itemData.shape.x + itemData.width / 2,
            itemData.shape.y + itemData.height / 2);
    }

    /**
     * @private
     */
    _initTacoPosition() {
        this._tacoPosition = new Phaser.Point(
            this._itemsManager.tacoData.shape.x,
            this._itemsManager.tacoData.shape.y);
    }

    /**
     * @private
     */
    _itemsManagerOnCustomerReceivedBeverage() {
        this._wasBeverageReceived = true;
        this._handleNextItem(BeverageKey);
    }

    /**
     * @private
     */
    _itemsManagerOnIngredientDroppedOnTaco(itemKey: string) {
        this._handleNextItem(itemKey);
    }

    /**
     * @private
     */
    _itemsManagerOnTacoDiscarded() {
        this._wasTacoDiscarded = true;
        this._destroyBubbleImage();
        this._presentCustomerOrder();
        this._highlightOrderItem(0);
    }

    /**
     * @private
     */
    _onCustomerComplete() {
        this._destroyBubbleImage();
        this.onComplete.dispatch();
    }

    /**
     * @private
     */
    _onTimerHilightIngredientsComplete() {
        this._destroyBubbleImage();
        this._highlightOrderItem(0);
    }

    /**
     * @private
     */
    _onTimerPresentCustomerComplete() {
        this._presentBubble(0, 220, 20);
        
        this._timer.add(2000, function() {
            this._onTimerPresentLookBubbleComplete();
        }, this);
    }

    /**
     * @private
     */
    _onTimerPresentGiveTacoComplete() {
        this._tutorialArrow.presentFromPosition(
            this._tacoPosition,
            this._customerPosition);
        this._tutorialArrow.visible = true;

        this._itemsManager.enableOnly(TacoKey);
    }

    /**
     * @private
     */
    _onTimerPresentIngredientsBubbleComplete() {
        this._presentBubble(1, 90, 380);
        this._timer.add(2000, function() {
            this._onTimerHilightIngredientsComplete();
        }, this);
    }

    /**
     * @private
     */
    _onTimerPresentLookBubbleComplete() {
        this._destroyBubbleImage();
        this._presentCustomerOrder();

        this._timer.add(2000, function() {
            this._onTimerPresentIngredientsBubbleComplete();
        }, this);
    }

    /**
     * @private
     */
    _presentBubble(bubbleImageIndex: number, x: number, y: number) {
        this._destroyBubbleImage();
        this._bubbleImage = this._game.add.image(x, y, 'tutorial', `tutorial-${bubbleImageIndex}`);
    }

    /**
     * @private
     */
    _presentCustomerOrder() {
        const USE_TIMER = false;
        this._customer.presentOrder(USE_TIMER);
    }

    /**
     * @private
     */
    _presentCustomer() {
        const CUSTOMER_ID = 'customer0';
        const ORDER_ITEMS = this._getTutorialOrderItems();

        this._customersManager.presentTutorialCustomer(
            CUSTOMER_ID,
            Tutorial._CUSTOMER_SLOT_INDEX,
            ORDER_ITEMS);

        this._customer =
            this._gameState.getCustomerBySlotIndex(Tutorial._CUSTOMER_SLOT_INDEX);
        this._customer.onComplete.addOnce(this._onCustomerComplete, this);

        this._customerPosition = new Phaser.Point(
            this._customer.centerX, this._customer.centerY);
    }

    /**
     * @private
     */
    _removeItemsManagerSignals() {
        this._itemsManager.onIngredientDroppedOnTaco.remove(
            this._itemsManagerOnIngredientDroppedOnTaco, this);
        this._itemsManager.onCustomerReceivedBeverage.remove(
            this._itemsManagerOnCustomerReceivedBeverage, this);
        this._itemsManager.onTacoDiscarded.remove(
            this._itemsManagerOnTacoDiscarded, this);
    }
}
