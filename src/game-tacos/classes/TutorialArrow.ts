export default class TutorialArrow extends Phaser.Image {
    _tween: Phaser.Tween;
    _tacoPosition: Phaser.Point;

    /**
     * @constructor
     */
    constructor(game: Phaser.Game) {
        super(game, 0, 0, 'tutorial', 'tutorial-arrow');
        this.anchor.set(0.5);
        
        const guiGroup: Phaser.Group = this.game['groups']['gui'];
        guiGroup.add(this);
    }

    destroy() {
        this._destroyTween();
        super.destroy();
    }

    presentFromPosition(fromPosition: Phaser.Point, toPosition: Phaser.Point) {
        this._destroyTween();

        this.x = fromPosition.x;
        this.y = fromPosition.y;
        this.rotation = Phaser.Math.angleBetweenPoints(fromPosition, toPosition);

        this._tween = this.game.tweens.create(this);

        const DURATION = 1000;
        const AUTO_START = true;
        const DELAY = 500;
        const REPEAT = -1;

        this._tween.to({
                x: toPosition.x,
                y: toPosition.y
            },
            DURATION,
            null,
            AUTO_START,
            DELAY,
            REPEAT);
    }

    _destroyTween() {
        if (this._tween) {
            this._tween.stop(false);
            this._tween = null;
        }
    }
}
