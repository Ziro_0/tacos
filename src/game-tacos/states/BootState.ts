export default class BootState {
    game: any;

    constructor(game) {
        this.game = game;
    }

    init() {
        // Responsive scaling
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        // Center the game
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;
    }

    preload() {
        this.game.load.image('preload_bar', 'assets/game-tacos/images/preload_bar.png');
    }

    create() {
        this.game.state.start('PreloadState');
    }
}
