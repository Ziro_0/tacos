import ItemsManager from '../classes/ItemsManager';
import Customer from '../classes/Customer';
import { Game } from '../game';
import Coins from '../classes/Coins';
import CustomersManager from '../classes/CustomersManager';
import Tutorial from '../classes/Tutorial';

export default class GameState  {
    game: Game;
    groups: any;
    
    _itemsManager: ItemsManager;
    _customersManager: CustomersManager;
    _tutorial: Tutorial;

    _backgroundImageGroup: Phaser.Group;
    _itemsGroup: Phaser.Group;
    _customersGroup: Phaser.Group;
    _customersMask: Phaser.Graphics;
    _customersMaskRect: Phaser.Rectangle;

    _gameScale: number;

    constructor(game: Game) {
        this.game = game;
        this._gameScale = 1.0;
    }

    create() {
        console.log('Game state [create] started');
        
        this._callListenerMap('ready', this.game);

        this._backgroundImageGroup = this.game.add.group();
        this._customersGroup = this.game.add.group();
        this._itemsGroup = this.game.add.group();

        this.game["groups"] = {
            gui: this.game.add.group()
        };
        
        this._createBackgroundImage();
        this._createCustomersMask();
        this._createItemsManager();
        this._createCustomersManager();

        if (this.game.config.skipTutorial) {
            this._startGamePlay();
        } else {
            this._createTutorial();
        }
    }
    
    getCustomerBySlotIndex(slotIndex: number): Customer {
        for (let index = 0; index < this._customersGroup.length; index++) {
            const customer: Customer = this._customersGroup.getAt(index) as Customer;
            if (customer.slotIndex === slotIndex) {
                return(customer);
            }
        }
        return(null);
    }

    shutdown() {
        this._customersManager.stop();
        this._disposeTutorial();
        Customer.ClearCaches();
    }

    update() {
        if (this._itemsManager) {
            this._itemsManager.update();
        }
    }

    /**
     * @private
     */
    _callListenerMap(key: string, ...args) {
        const callback:Function = this.game.listenerMapping[key];
        if (callback) {
            callback.apply(null, args);
        }
    }

    /**
     * @private
     */
    _createBackgroundImage() {
        let image = this.game.add.image(
            this.game.world.centerX,
            this.game.world.centerY,
            'atlas',
            'background',
            this._backgroundImageGroup);
        image.anchor.set(0.5);

        image.width = this.game.world.width;
        image.scale.y = image.scale.x;
        
        if (image.height < this.game.world.height) {
            image.height = this.game.world.height;
            image.scale.x = image.scale.y;
        }

        this._gameScale = image.scale.x;
    }

    /**
     * @private
     */
    _createCustomer(customerId: string, customerSlotIndex: number,
    orderItemKeys: string[], time: number, isTutorialMode: boolean): Customer {
        //sanity checks
        if (!customerId) {
            console.warn('Unable to get ingredient keys to create customer.')
            return;
        } else if (!orderItemKeys || orderItemKeys.length === 0) {
            console.warn('Unable to get ingredient keys to create customer.')
            return;
        };

        const isComingFromLeft = Math.random() >= 0.5;
        const customer = new Customer(this.game, customerId, orderItemKeys,
            isComingFromLeft, customerSlotIndex, time, this._customersGroup,
            this._customersMask, isTutorialMode);

        customer.onComplete.addOnce(this._onCustomerComplete, this);

        if (!isTutorialMode) {
            this.game.sound.play('new-customer');
        }

        return(customer);
    }

    /**
     * @private
     */
    _createCustomersManager() {
        this._customersManager = new CustomersManager(
            this.game,
            this.game.config.minCustomerSpawnRateMs,
            this.game.config.maxCustomerSpawnRateMs,
            this.game.config.minCustomerTime,
            this.game.config.maxCustomerTime,
            this.game.config.firstCustomerSpawnTimeMs);
        
        this._customersManager.onNewCustomer.add(
            this._onCustomersManagerNewCustomer, this);
    }

    /**
     * @private
     */
    _createCustomersMask() {
        const CUSTOMERS_AREA_WIDTH = 640;
        const CUSTOMERS_AREA_HEIGHT = 443;
        
        this._customersMaskRect = new Phaser.Rectangle(0, 0,
            CUSTOMERS_AREA_WIDTH * this._gameScale,
            CUSTOMERS_AREA_HEIGHT * this._gameScale);
        
        this._customersMask = this.game.make.graphics();
        this._customersMask.beginFill(0x00ff00, 0.5);
        this._customersMask.drawRect(
            this._customersMaskRect.x,
            this._customersMaskRect.y,
            this._customersMaskRect.width,
            this._customersMaskRect.height);
        this._customersMask.endFill();
    }

    /**
     * @private
     */
    _createItemsManager() {
        this._itemsManager = new ItemsManager(
            this.game,
            this._gameScale,
            this._itemsGroup,
            this._customersGroup,
            this._customersMaskRect);

        this._itemsManager.onCustomerReceivedTaco.add(
            this._onItemsManagerCustomerReceivedTaco, this);
        
        this._itemsManager.onCustomerReceivedBeverage.add(
            this._onItemsManagerCustomerReceivedBeverage, this);
    }

    /**
     * @private
     */
    _createCoins(customer: Customer, value: number) {
        const coins = new Coins(this.game, customer, value);
        coins.onCollected.addOnce(this._onCoinsCollected, this);
        this._itemsGroup.add(coins);
    }

    /**
     * @private
     */
    _createTutorial() {
        this._tutorial = new Tutorial(this, this._customersManager, this._itemsManager);
        this._tutorial.onComplete.addOnce(this._onTutorialComplete, this);
        this._tutorial.start();
    }

    /**
     * @private
     */
    _disposeTutorial() {
        if (this._tutorial) {
            this._tutorial.dispose();
            this._tutorial = null;
        }
    }

    /**
     * @private
     */
    _handleOrderSuccess(customer: Customer, moneyValue: number) {
        this.game.sound.play('order-success');
        this._createCoins(customer, moneyValue);
    }

    /**
     * @private
     */
    _handleOrderFail(customer: Customer) {
        this.game.sound.play('order-fail');
        this._customersManager.clearCustomerSlot(customer.slotIndex);
        this._callListenerMap('lives-lost', this.game);
    }

    /**
     * @private
     */
    _onCoinsCollected(coins: Coins) {
        this._customersManager.clearCustomerSlot(coins.slotIndex);
        this._customersManager.restartSpawner();
        this.game.sound.play('collect-coins');
        this._callListenerMap('add-point', coins.value);
    }

    /**
     * @private
     */
    _onCustomerComplete(customer: Customer, isOrderSuccessful: boolean, value: number) {
        if (isOrderSuccessful) {
            this._handleOrderSuccess(customer, value);
        } else {
            this._handleOrderFail(customer);
        }
    }

    /**
     * @private
     */
    _onCustomersManagerNewCustomer(customerId: string, customerSlotIndex: number,
    orderItemKeys: string[], time: number, isTutorialMode: boolean = false) {
        this._createCustomer(customerId, customerSlotIndex, orderItemKeys,
            time, isTutorialMode);
    }

    /**
     * @private
     */
    _onItemsManagerCustomerReceivedBeverage(customerSlotIndex: number) {
        const customer = this.getCustomerBySlotIndex(customerSlotIndex);
        if (customer && customer.isWaiting) {
            customer.giveBeverage();
        }
    }

    /**
     * @private
     */
    _onItemsManagerCustomerReceivedTaco(customerSlotIndex: number, tacoIngredientKeys: string[]) {
        const customer = this.getCustomerBySlotIndex(customerSlotIndex);
        if (customer && customer.isWaiting) {
            customer.giveTaco(tacoIngredientKeys);
        }
    }

    /**
     * @private
     */
    _onTutorialComplete() {
        this._itemsManager.enableAll();
        this._disposeTutorial();
        this._startGamePlay();
    }

    /**
     * @private
     */
    _startGamePlay() {
        this._customersManager.start();
    }
}
