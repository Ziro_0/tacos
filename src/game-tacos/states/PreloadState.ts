export default class PreloadState {
    game: Phaser.Game;
    preloadBar: Phaser.Image;

    constructor(game: Phaser.Game) {
        this.game = game;
    }

    preload() {
        this.preloadBar = this.game.add.image(
            this.game.world.centerX - 200,
            this.game.world.centerY - 200,
            'preload_bar');

        this.game.load.setPreloadSprite(this.preloadBar);

        this.game.load.image('heart', 'assets/game-tacos/images/ui/heart.png');

        this.game.load.atlasJSONArray(
            'atlas',
            'assets/game-tacos/images/atlas-0.png',
            'assets/game-tacos/images/atlas-0.json');

        this.game.load.atlasJSONArray(
            'trash',
            'assets/game-tacos/images/trash.png',
            'assets/game-tacos/images/trash.json');

        this.game.load.atlasJSONArray(
            'tutorial',
            'assets/game-tacos/images/tutorial.png',
            'assets/game-tacos/images/tutorial.json');

        this._loadCustomerImages(0, 3);
        this._loadCustomerImages(1, 2);
        this._loadCustomerImages(2, 3);
        this._loadCustomerImages(3, 2);
        this._loadCustomerImages(4, 3);
        this._loadCustomerImages(5, 3);

        this.game.load.json('items', 'assets/game-tacos/data/items.json');
        this.game.load.json('customers', 'assets/game-tacos/data/customers.json');

        this.game.load.audio('collect-coins', 'assets/game-tacos/sounds/collect-coins.mp3');
        this.game.load.audio('new-customer', 'assets/game-tacos/sounds/new-customer.mp3');
        this.game.load.audio('order-fail', 'assets/game-tacos/sounds/order-fail.mp3');
        this.game.load.audio('order-success', 'assets/game-tacos/sounds/order-success.mp3');
    }

    create() {
        this.game.state.start('GameState');
    }

    /**
     * @private
     */
    _loadCustomerImages(customerIndex: number, numberOfAtlases: number) {
        const BASE_PATH = 'assets/game-tacos/images/customers/';
        for (let index = 0; index < numberOfAtlases; index++) {
            const key = `customer${customerIndex}-${index}`;
            this.game.load.image(key, `${BASE_PATH}${key}.png`);
        }
    }
}
